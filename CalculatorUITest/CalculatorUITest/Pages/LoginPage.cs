﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;


namespace CalculatorUITest.Pages
{
    public class LoginPage : BasePage
    {
        #region Queries
        readonly Query _loginButton;
        readonly Query _emailInput;
        readonly Query _passwordInput;
        readonly Query _errorMessage;
        #endregion

        #region Locators
        private string _welcomeTitleID = "automation_welcomeID";
        private string _loginID        = "automation_loginButtonID";
        private string _emailID        = "automation_emailID";
        private string _passwordID     = "automation_passwordID";
        private string _errorID        = "automation_errorID";
        #endregion
        protected override PlatformQuery Trait => new PlatformQuery
        {
            //must exist element in order to be able to start all  related tests
            Android = x => x.Marked(_welcomeTitleID),
            iOS     = x => x.Marked(_welcomeTitleID)
        };

        public LoginPage()
        {
            _loginButton   = x => x.Marked(_loginID);
            _emailInput    = x => x.Marked(_emailID);
            _passwordInput = x => x.Marked(_passwordID);
            _errorMessage  = x => x.Marked(_errorID);
        }

        public void Login(string email, string password)
        {
            Thread.Sleep(2000);
            EnterText(_emailInput, email);
            PressEnter();
            WaitForElement(_passwordInput);
            EnterText(_passwordInput, password);
            PressEnter();
            Tap(_loginButton);
        }

        public string GetInvalidLoginMessage()
        {
            WaitForElement(_errorMessage);
            return GetElementText(_errorMessage);
        }

    }
}
