﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;

namespace CalculatorUITest.Pages
{
    public class DashboardPage : BasePage
    {
        #region Queries
        readonly Query _subTitle;
        readonly Query _openCalculatorButton;
        #endregion

        #region AutomationIDs
        private string _dashboardTile    = "automation_dashboardID";
        private string _subTitleID       = "automation_subTitleID";
        private string _openCalculatorID = "automation_OpenCalcPageButtonID";
        #endregion

        protected override PlatformQuery Trait => new PlatformQuery
        {
            //must exist element in order to be able to start all  related tests. The login button
            Android = x => x.Marked(_dashboardTile),
            iOS     = x => x.Marked(_dashboardTile)
        };

        public DashboardPage()
        {
            Thread.Sleep(2000);
            _subTitle             = x => x.Marked(_subTitleID);
            _openCalculatorButton = x => x.Marked(_openCalculatorID);
        }

        public string GetSubTitleInformation()
        {
            return GetElementText(_subTitle);
        }

        public CalculatorPage OpenCalculatorPage()
        {
            Tap(_openCalculatorButton);
            return new CalculatorPage();
        }
    }
}
