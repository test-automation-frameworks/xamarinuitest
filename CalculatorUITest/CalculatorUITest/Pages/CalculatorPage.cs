﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;

namespace CalculatorUITest.Pages
{
    public class CalculatorPage : BasePage
    {
        #region Queries
        readonly Query _sumButton;
        readonly Query _subtractionButton;
        readonly Query _divideButton;
        readonly Query _multipleButton;
        readonly Query _inputNumberX;
        readonly Query _inputNumberY;
        readonly Query _resultInput;
        #endregion

        #region AutomationIDs
        private string _sumID          = "automation_sumButtonID";
        private string _subtrasctionID = "automation_subtractionButtonID";
        private string _divideID       = "automation_divideButtonID";
        private string _multipleID     = "automation_multipleButtonID";
        private string _titleText      = "Calculator";
        private string _numberXID      = "automation_numberXID";
        private string _numberYID      = "automation_numberYID";
        private string _resultID       = "automation_resultID";
        #endregion

        protected override PlatformQuery Trait => new PlatformQuery
        {
            //must exist element in order to be able to start all  related tests
            Android = x => x.Text(_titleText),
            iOS     = x => x.Text(_titleText)
        };

        public CalculatorPage()
        {
            Thread.Sleep(2000);
            _subtractionButton = x => x.Marked(_subtrasctionID);
            _sumButton         = x => x.Marked(_sumID);
            _divideButton      = x => x.Marked(_divideID);
            _multipleButton    = x => x.Marked(_multipleID);
            _inputNumberX      = x => x.Marked(_numberXID);
            _inputNumberY      = x => x.Marked(_numberYID);
            _resultInput       = x => x.Marked(_resultID);
        }

        private void EnterDataIntoTheNumberInputs(string numberA, string numberB)
        {
            EnterText(_inputNumberX, numberA);
            EnterText(_inputNumberY, numberB);
        }
        
        private string GetResultOperation()
        {
            WaitForElement(_resultInput);
            return GetElementText(_resultInput);
        }

        public string SumOperation(string numberA, string numberB)
        {
            EnterDataIntoTheNumberInputs(numberA, numberB);
            Tap(_sumButton);
            return GetResultOperation();
        }

        public string SubtractionOperation(string numberA, string numberB)
        {
            EnterDataIntoTheNumberInputs(numberA, numberB);
            Tap(_subtractionButton);
            return GetResultOperation();
        }

        public string DivideOperation(string numberA, string numberB)
        {
            EnterDataIntoTheNumberInputs(numberA, numberB);
            Tap(_divideButton);
            return GetResultOperation();
        }

        public string MultipleOperation(string numberA, string numberB)
        {
            EnterDataIntoTheNumberInputs(numberA, numberB);
            Tap(_multipleButton);
            return GetResultOperation();
        }
    }
}
