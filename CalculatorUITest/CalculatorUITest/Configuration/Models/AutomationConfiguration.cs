﻿using System.Collections.Generic;

namespace WorkerAppMobileTestAutomation.Configuration.Models
{
    public class AutomationConfiguration
    {
        public string SystemUnderTest { get; set; }
        public Dictionary<string, NotificationMethod> Notifications { get; set; }
        public ExecutionSettings ExecutionSettings { get; set; }
        public Dictionary<string, Database> Databases { get; set; }

        public Dictionary<string, EnvironmentSetting> EnvironmentsSettings { get; set; }

    }

}
