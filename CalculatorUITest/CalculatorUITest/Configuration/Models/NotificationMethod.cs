﻿namespace WorkerAppMobileTestAutomation.Configuration.Models
{
    public class NotificationMethod
    {
        public string Channel { get; set; }
        public string XmlResultsPath { get; set; }
    }
}
