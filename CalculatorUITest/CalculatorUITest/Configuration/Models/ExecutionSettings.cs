﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerAppMobileTestAutomation.Configuration.Models
{
    public class ExecutionSettings
    {   public string Environment { get; set; }
        public WaitSettings Waits { get; set; }
        public string Database { get; set; }
    }
}
