﻿namespace WorkerAppMobileTestAutomation.Configuration.Models
{
    public class WaitSettings
    {
        public int TimeoutSeconds { get; set; }
    }
}
