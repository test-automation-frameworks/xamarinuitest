﻿namespace WorkerAppMobileTestAutomation.Configuration.Models
{
    public class EnvironmentSetting
    {
        public string IosBundleId { get; set; }
        public string IosDeviceIdentifier { get; set; }
        public string AndroidApkFullPath { get; set; }

    }
}
