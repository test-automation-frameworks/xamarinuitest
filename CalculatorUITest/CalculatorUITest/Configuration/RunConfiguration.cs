﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkerAppMobileTestAutomation.Configuration.Models;

namespace CalculatorUITest.Configuration
{
    public class RunConfiguration
    {
        public readonly string Env;
        public readonly int Timeout;
        public readonly string AndroidApkFullPath;
        public readonly string IosBundleId;
        public readonly string IosDeviceIdentifier;


        private static RunConfiguration _runConfiguration;
        private static string _automationConfigResourceName = "CalculatorUITest.Configuration.AutomationConfig.json";
        private AutomationConfiguration _configuration;

        public static RunConfiguration ReadConfiguration()
        {
            if (null != _runConfiguration) return _runConfiguration;

            string jsonResource = ResourceReader.GetResource(_automationConfigResourceName);

            var configuration = ConfigReader.ReadConfigurationFromJsonObject(jsonResource);
            _runConfiguration = new RunConfiguration(configuration);

            return _runConfiguration;
        }

        public static RunConfiguration GetConfiguration()
        {
            return _runConfiguration;
        }

        private RunConfiguration(AutomationConfiguration config)
        {
            Env = config.ExecutionSettings.Environment;
            Timeout = config.ExecutionSettings.Waits.TimeoutSeconds;
            AndroidApkFullPath = config.EnvironmentsSettings[Env].AndroidApkFullPath;
            IosBundleId = config.EnvironmentsSettings[Env].IosBundleId;
            IosDeviceIdentifier = config.EnvironmentsSettings[Env].IosDeviceIdentifier;
            _configuration = config;
        }
    }
}
