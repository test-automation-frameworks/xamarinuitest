﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorUITest.Configuration
{
    public static class ResourceReader
    {
        public static string GetResource(string resourceName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            //var resourcesList = assembly.GetManifestResourceNames(); In case you want to know what are the resources names
            string resource = null;
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                resource = reader.ReadToEnd();
            }
            resource.ToString();
            return resource;
        }
    }
}
