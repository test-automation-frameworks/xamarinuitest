﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkerAppMobileTestAutomation.Configuration.Models;

namespace CalculatorUITest.Configuration
{
    public class ConfigReader
    {
        public static AutomationConfiguration ReadConfigurationFromJsonObject(string json)
        {
            return JsonConvert.DeserializeObject<AutomationConfiguration>(json);
        }

    }
}
