﻿using CalculatorUITest.Pages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.UITest;

namespace CalculatorUITest.Test
{
    [TestFixture(Platform.Android)]
    //[TestFixture(Platform.iOS)]
    [Category("LoginTests")]
    public class LoginTest : BaseTestFixture
    {
        public LoginTest(Platform platform): base(platform)
        {
        }

        [Test]
        public void LoginExistingAccountTest()
        {
            string email    = "Jeancarlo@gmail.com";
            string password = "Admin@123";

            this.Login(email, password);
           
            var dashboardPage = new DashboardPage();
            string currentSubtitle = dashboardPage.GetSubTitleInformation();
            string expectedSubtitle = "Xamarin UI Test";

            Assert.AreEqual(expectedSubtitle, currentSubtitle);
        }

        [Test]
        public void InvalidLoginTest()
        {
            string email    = "invalidAccount@gmail.com";
            string password = "invalidPassword";

            this.Login(email, password);

            string expectedErrorMessage = "Email or password invalid!";
           
            Assert.AreEqual(expectedErrorMessage, new LoginPage().GetInvalidLoginMessage());
        }

        //[Test]
        //public void Repl()
        //{
        //    string email = "Jeancarlo@gmail.com";
        //    string password = "Admin@123";

        //    //this.Login(email, password);
        //    app.Repl();
        //}
    }
}
