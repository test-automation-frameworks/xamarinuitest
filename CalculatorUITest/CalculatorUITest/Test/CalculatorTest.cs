﻿using CalculatorUITest.Pages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.UITest;

namespace CalculatorUITest.Test
{
    [TestFixture(Platform.Android)]
    //[TestFixture(Platform.iOS)]
    public class CalculatorTest : BaseTestFixture
    {
        private CalculatorPage calculatorPage;
        public CalculatorTest(Platform platform) : base(platform)
        {
        }

        [SetUp]
        public void Login()
        {
            string email = "Jeancarlo@gmail.com";
            string password = "Admin@123";
         
            this.Login(email, password);
            this.calculatorPage = new DashboardPage().OpenCalculatorPage();
        }

        [Test]
        public void SumTest()
        {
           
            string expectedResult = "22";
            string result = this.calculatorPage.SumOperation("10", "12");
            
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void SubtractionTest()
        {
            string expectedResult = "20";

            Assert.AreEqual(expectedResult, this.calculatorPage.SubtractionOperation("30", "10"));
        }

        [Test]
        public void DivideTest()
        {
            string expectedResult = "50";

            Assert.AreEqual(expectedResult, this.calculatorPage.DivideOperation("500", "10"));
        }

        [Test]
        public void MultipleTest()
        {
            string expectedResult = "200";

            Assert.AreEqual(expectedResult, this.calculatorPage.MultipleOperation("20", "10"));
        }

        [Test]
        public void DisplayErrorMessageTest()
        {
            string expectedResult = "Please set only numbers in the inputs!";

            Assert.AreEqual(expectedResult, this.calculatorPage.SumOperation("invalidInput", "invaldInput2"));
        }


    }
}
