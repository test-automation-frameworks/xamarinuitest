﻿using CalculatorUITest.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.UITest;

namespace CalculatorUITest
{
    static class AppManager
    {
        static RunConfiguration RunConfiguration;

        static IApp app;
        public static IApp App
        {
            get
            {
                if (app == null)
                    throw new NullReferenceException("'AppManager.App' not set. Call 'AppManager.StartApp()' before trying to access it.");
                return app;
            }
        }

        static Platform? platform;
        public static Platform Platform
        {
            get
            {
                if (platform == null)
                    throw new NullReferenceException("'AppManager.Platform' not set.");
                return platform.Value;
            }

            set
            {
                platform = value;
            }
        }

        public static void StartApp()
        {
            RunConfiguration = RunConfiguration.ReadConfiguration();
            if (Platform == Platform.Android)
            {
                app = ConfigureApp
                    .Android
                    // .EnableLocalScreenshots()
                    // Used to run a .apk file:
                    // .ApkFile(Path.Combine(binariesFolder, "<>.apk"))
                    .ApkFile(RunConfiguration.AndroidApkFullPath)
                    .StartApp();
            }
            
            if (Platform == Platform.iOS)
            {
                app = ConfigureApp
                    .iOS
                    // Used to run a .app file on an ios simulator:
                    //.AppBundle(Path.Combine(binariesFolder, "TaskyiOS.app"))
                    // Used to run a .ipa file on a physical ios device:
                    //use if you want to point to a simulator => ids=> terminal=> xcrun simctl list --json
                    //.DeviceIdentifier("0435CCA3-DFAD-4F42-A65F-9D22657350CC")
                    //if deviced has been plugged in it will try to launch tests from a physical device
                    

                    .InstalledApp(RunConfiguration.IosBundleId)
                    .StartApp();
            }
            
        }
    }
}
