# Calculator Testing Framework with XamarinUITest

In the following links you can find a better explanation of the project and also examples of App Center execution

[Cloud-Based Automated Testing: A Tutorial With Xamarin.UITest and App Center (Part 1)](https://gorillalogic.com/blog/cloud-based-automated-testing-a-tutorial-with-xamarin-uitest-and-app-center-part-1)


[Cloud-Based Automated Testing: A Tutorial With Xamarin.UITest and App Center (Part 2)](https://gorillalogic.com/blog/cloud-based-automated-testing-a-tutorial-with-xamarin-uitest-and-app-center-part-2)
